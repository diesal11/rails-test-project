# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user1 = User.create(email: 'user1@test.com', password: 'aaa', admin: false)
user2 = User.create(email: 'user2@test.com', password: 'aaa', admin: true)

post1 = Post.create(title: "My First Post", content: "<b>Some bold text</b><br />This is a new line!", user_id: User.find_by(email: 'user1@test.com').id)
post2 = Post.create(title: "The Second Post", content: "<b>This post</b><br />Is the second post created! and it is posted by a different user", user_id: user2.id)

for i in 3..10
	Post.create(title: "Post #{i}", content: "<b>Some bold text</b><br />This is a new line!", user_id: user1.id)
end