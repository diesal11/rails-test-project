class PostsController < ApplicationController
  before_action :require_admin, only: [ :edit, :update, :delete, :destroy]

  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id
    if @post.save
      redirect_to(:action => 'index')
    else
      render('new')
    end
  end

  def show
    @post = Post.find(params[:id])
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(post_params)
      redirect_to(:action => 'show', :id => @post.id)
    else
      render('index')
    end
  end

  def delete
    @post = Post.find(params[:id])
  end

  def destroy
    Post.find(params[:id]).destroy
    redirect_to(:action => 'index')
  end

private

  def post_params
    params.require(:post).permit(:title, :content)
  end
end
